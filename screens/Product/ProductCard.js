import { View, Text, StyleSheet, Dimensions, Image, Button } from "react-native";

const {width} = Dimensions.get("window");

function ProductCard({ product }) {
    return ( 
    <View style={styles.rootContainer}>
       <View style ={styles.imageContainer}>
       <Image style= {styles.image} 
        source={{
            uri: product.image ? 
            product.image: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pinterest.com%2Farianna_717%2Fstitch%2F&psig=AOvVaw2iZhlhlhXOlLPBeMqgWYB_&ust=1680840419010000&source=images&cd=vfe&ved=0CA0QjRxqFwoTCMCtqtawlP4CFQAAAAAdAAAAABAY" }} />
       </View>

        <Text style={styles.title}>{product.brand}</Text>
        <Text style={styles.price}>Nu. {product.price}</Text>
            {product.countInStock > 0 ? 
            <Button title= "ADD" color="green"/> : 
            <Text style={styles.statusText}>Currently not Avaiable</Text>}
    </View>
    );
}

export default ProductCard;

const styles = StyleSheet.create({
    rootContainer:{
        width: width / 2 -20,
        height: width/1.7,
        backgroundColor: "white",
        padding: 10,
        marginTop: 60,
        marginBottom: 20,
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 10,
        borderRadius: 20,
    },
    title: {
        fontWeight: "bold",
        fontSize: 12,
        marginTop: 100,
    },
    price: {
        color: "orange",
        fontSize: 18,
    },
    image:{
        width: "100%",
        height: "100%",

    },
    imageContainer:{
        width: width / 2 -20 - 20,
        height: width /2 -20 -30,
        top: -45,
        position:"absolute",
        borderRadius: 20,
        overflow: "hidden",

    }
})